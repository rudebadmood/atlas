#!/usr/bin/perl

use Text::CSV;
use Data::Dumper;

my %projects;
my $projectsfile = 'projects.csv';
my $csv = Text::CSV->new ({ binary => 1});
open my $fh, '<', $projectsfile or die "Could not open $projectsfile: $!";

while( my $row = $csv->getline( $fh ) ) { 
    $projects{@$row[0]} = [@$row[1], @$row[2]] ;
}

# print Dumper %projects;

my $filename = shift or die "Usage: $0 FILENAME\n";

$rulesout = $filename;
$rulesout =~ s/\.json$/\.csv/;

binmode OUT, ":utf8";

open OUT, '>', $rulesout;

$jqout = `jq -r '.rules[] | [.name, .description, .state, .id, .projects[0].projectId] | \@csv' $filename` ;

open my $jq_fh, '<', \$jqout;

print OUT "Name, Description, Enabled, Key, Project\n";

while( my $row = $csv->getline( $jq_fh ) ) { 
	($name, $description, $enabled, $id, $projid) = @$row;
	$name =~ s#\|#\\|#g;
	$name =~ s/"/""/g;
	$name =~ s/([{}])/\\${1}/g;

	$description =~ s/"/""/g;
	$description =~ s/([{}])/\\${1}/g;

	$key = $projects{$projid}[0];

	$projname = $projects{$projid}[1];
	$projname =~ s/"/""/g;
	$projname =~ s/([{}])/\\${1}/g;

	if ($key eq '') { $key = 'DELETED'; }
	$url = 'https://YOURSITE.atlassian.net/plugins/servlet/ac/com.codebarrel.addons.automation/cb-automation-project-config?project.key=' . $key . '&project.id=' . $projid . '#/rule/' . $id;
	print OUT qq#"[$name | $url]", "$description", $enabled, $key, "$projname"\n#;
}

close OUT;

